'use strict'
const Mail = use('Mail')

class NewSubscriptionMail {
  static get concurrency () {
    return 1
  }

  static get key () {
    return 'NewSubscriptionMail-job'
  }

  async handle ({ user, meetup }) {
    await Mail.send(
      ['emails.subscription_confirmation'],
      {
        username: user.name,
        title: meetup.title,
        localization: meetup.localization,
        url: `http://localhost:3000/meetup/${meetup.id}`
      },
      message => {
        message
          .to(user.email)
          .from('suporte@meetups.com.br')
          .subject('Vaga garantida!')
      }
    )
  }
}

module.exports = NewSubscriptionMail
